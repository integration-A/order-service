package com.dominos.resource;

import static org.junit.Assert.*;

import javax.ws.rs.core.Response;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.dominos.req.Order;

public class OrderResourceTest {
	private OrderResource OrderResource = new OrderResource();

	@Before
	public void setUp() throws Exception {
		 
	}

	@After
	public void tearDown() throws Exception {
		
		
	}

	@Test
	public void testCreateOrder() {
		Order order = new Order();
		
		
		Response response = OrderResource.createOrder(order);
		String expected ="created succesfully";
		assertEquals(expected, response.getEntity());
		
	}

}
